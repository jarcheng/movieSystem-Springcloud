package com.jarcheng.order.controller;


import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.OrderPayment;
import com.jarcheng.mbg.entity.UserOrder;
import com.jarcheng.order.service.UserOrderService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping("/order")
@Api(tags = "订单相关")
@RestController
public class UserOrderController {
    @Autowired
    UserOrderService orderService;

    @GetMapping("/selectOrderById")
    @RequiresRoles("admin")
    public Result selectOrderById(@RequestParam(value = "id", defaultValue = "0") Integer id) {
        return Result.SUCCESS(orderService.selectByPrimaryKey(id));
    }

    @PostMapping("/generateOrder")
    public Result generateOrder(@RequestBody UserOrder userOrder) {
        return Result.SUCCESS(orderService.insertSelective(userOrder));
    }

    @GetMapping("/deleteOrder")
    @RequiresRoles("admin")
    public Result deleteOrder(@RequestParam(value = "id", defaultValue = "0") Integer id) {
        return Result.SUCCESS(orderService.deleteByPrimaryKey(id));
    }

    @PostMapping("/payment")
    public Result payment(@RequestBody OrderPayment orderPayment) {
        if (orderService.orderPayment(orderPayment) == 0) {
            return Result.FAIL();
        }
        return Result.SUCCESS();
    }

    @GetMapping("/getUserOrder")
    public Result getUserOrder() {
        return Result.SUCCESS(orderService.selectByUserOrder());
    }

    @PostMapping("/updateOrder")
    public Result updateOrder(@RequestBody UserOrder order) {
        return Result.SUCCESS(orderService.updateByPrimaryKeySelective(order));
    }

    @GetMapping("/selectAllOrder")
    @RequiresRoles("admin")
    public Result selectAllOrder() {
        return Result.SUCCESS(orderService.selectAll());
    }
}
