package com.jarcheng.order.rabbitmq;



import com.jarcheng.mbg.entity.UserOrder;
import com.jarcheng.order.service.UserOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "movie.order.cancel")
@Slf4j
public class MessageReceiver {
    @Autowired
    UserOrderService orderService;

    @RabbitHandler
    public void handle(Integer orderId) {
        UserOrder order = new UserOrder();
        order.setId(orderId);
        UserOrder order1 = orderService.selectByPrimaryKey(orderId);
        if (order1 == null) return;
        if (order1.getStatus() == 1) {
            return;
        }
        order.setStatus(2);
        orderService.updateByPrimaryKeySelective(order);
        log.info("已关闭订单");
    }

}