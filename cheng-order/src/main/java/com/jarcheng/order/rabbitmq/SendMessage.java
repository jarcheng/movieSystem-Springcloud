package com.jarcheng.order.rabbitmq;

import com.jarcheng.common.enums.QueueEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class SendMessage {
    @Autowired
    AmqpTemplate amqpTemplate;  //使用RabbitTemplate,这提供了接收/发送等等方法

    public void send(Integer orderId) {
        amqpTemplate.convertAndSend(QueueEnum.QUEUE_TTL_ORDER_CANCEL.getExchange(), QueueEnum.QUEUE_TTL_ORDER_CANCEL.getRouteKey(),
                orderId);
    }

}
