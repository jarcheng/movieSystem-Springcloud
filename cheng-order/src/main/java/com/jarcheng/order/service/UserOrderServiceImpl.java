package com.jarcheng.order.service;

import com.jarcheng.mbg.dao.OrderPaymentDao;
import com.jarcheng.mbg.dao.UserOrderDao;
import com.jarcheng.mbg.dao.UserRoleDao;
import com.jarcheng.mbg.entity.OrderPayment;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserOrder;
import com.jarcheng.mbg.entity.UserRole;
import com.jarcheng.order.rabbitmq.SendMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class UserOrderServiceImpl implements UserOrderService {
    @Autowired
    UserOrderDao orderDao;
    @Autowired
    SendMessage sendMessage;
    @Autowired
    OrderPaymentDao paymentDao;

    @Autowired
    UserRoleDao roleService;

    @Override
    public UserOrder insertSelective(UserOrder userOrder) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        userOrder.setUser_uuid(user.getUuid());
        userOrder.setOrder_sn(UUID.randomUUID().toString());
        userOrder.setUser_name(user.getName());
        orderDao.insertSelective(userOrder);
        sendMessage.send(userOrder.getId());
        log.info(userOrder.toString());
        return userOrder;
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return orderDao.deleteByPrimaryKey(id);
    }

    @Override
    public UserOrder selectByPrimaryKey(Integer id) {
        return orderDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(UserOrder record) {

        return orderDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserOrder selectByOrderSn(String orderSn) {
        return orderDao.selectByOrderSn(orderSn);
    }

    @Override
    public List<UserOrder> selectByUserOrder() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        return orderDao.selectByUserOrder(user.getUuid());
    }

    @Override
    public List<UserOrder> selectAll() {
        return orderDao.selectAll();
    }

    @Override
    public int orderPayment(OrderPayment orderPayment) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        orderPayment.setUser_uuid(user.getUuid());
        orderPayment.setUser_name(user.getName());

        UserOrder userOrder = selectByOrderSn(orderPayment.getOrder_sn());
        orderPayment.setOrderId(userOrder.getId());

        if (userOrder.getStatus() == 0 && userOrder.getPay_amount().intValue() == orderPayment.getAmount().intValue() &&
                userOrder.getUser_uuid().equals(orderPayment.getUser_uuid())) {
            log.info(orderPayment.toString());
            int insert = paymentDao.insert(orderPayment);
            userOrder.setStatus(1);
            userOrder.setPay_type(1);
            orderDao.updateByPrimaryKeySelective(userOrder);
            UserRole userRole = new UserRole();
            userRole.setUuid(user.getUuid());
            userRole.setName(user.getName());
            userRole.setRole("vip");
            roleService.insertSelective(userRole);
            return insert;
        }
        return 0;
    }
}
