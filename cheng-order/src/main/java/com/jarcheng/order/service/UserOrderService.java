package com.jarcheng.order.service;


import com.jarcheng.mbg.entity.OrderPayment;
import com.jarcheng.mbg.entity.UserOrder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface UserOrderService {
    @Transactional
    UserOrder insertSelective(UserOrder record);

    @Transactional
    int deleteByPrimaryKey(Integer id);

    UserOrder selectByPrimaryKey(Integer id);

    @Transactional
    int updateByPrimaryKeySelective(UserOrder record);
    UserOrder selectByOrderSn( String orderSn);

    List<UserOrder> selectByUserOrder();
    List<UserOrder> selectAll();

    @Transactional
    int orderPayment(OrderPayment payment);
}
