package com.jarcheng.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"com.jarcheng"})
public class ChengOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengOrderApplication.class, args);
    }

}
