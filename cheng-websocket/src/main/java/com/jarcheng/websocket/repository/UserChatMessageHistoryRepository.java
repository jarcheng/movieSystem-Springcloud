package com.jarcheng.websocket.repository;

import com.jarcheng.websocket.payload.SingleMessageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserChatMessageHistoryRepository extends MongoRepository<SingleMessageRequest, String> {
    List<SingleMessageRequest> findSingleMessageRequestByFromUidAndToUid(String fromUUid, String toUUid);
}
