package com.jarcheng.websocket.repository;

import com.jarcheng.websocket.payload.UserChatHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserChatHistoryRepository extends MongoRepository<UserChatHistory, String> {
    List<UserChatHistory> findUserChatHistoriesByMyUUIDOrderByDate(String myUUID);

    UserChatHistory findUserChatHistoriesByToUserAndMyUUID(String uuid1, String uuid2);
}
