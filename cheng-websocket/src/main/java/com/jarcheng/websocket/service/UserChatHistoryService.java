package com.jarcheng.websocket.service;

import com.jarcheng.websocket.payload.UserChatHistory;

import java.util.List;

public interface UserChatHistoryService {
    List<UserChatHistory> findUserAllChat(String fromUUID);

    UserChatHistory findSpecificChat(String uuid, String uuid2);

    void insertUserChat(UserChatHistory userChatHistory);

    void updateUserChat(UserChatHistory userChatHistory);

    void deleteUserChat(UserChatHistory userChatHistory);
}
