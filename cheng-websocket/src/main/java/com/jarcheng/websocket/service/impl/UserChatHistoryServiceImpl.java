package com.jarcheng.websocket.service.impl;

import com.jarcheng.websocket.payload.UserChatHistory;
import com.jarcheng.websocket.repository.UserChatHistoryRepository;
import com.jarcheng.websocket.service.UserChatHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserChatHistoryServiceImpl implements UserChatHistoryService {
    @Autowired
    UserChatHistoryRepository chatHistoryRepository;

    @Override
    public List<UserChatHistory> findUserAllChat(String fromUUD) {
        return chatHistoryRepository.findUserChatHistoriesByMyUUIDOrderByDate(fromUUD);
    }

    @Override
    public UserChatHistory findSpecificChat(String toUUid, String fromUUD) {
        return chatHistoryRepository.findUserChatHistoriesByToUserAndMyUUID(toUUid, fromUUD);
    }

    @Override
    public void insertUserChat(UserChatHistory userChatHistory) {
        chatHistoryRepository.save(userChatHistory);
    }

    @Override
    public void updateUserChat(UserChatHistory userChatHistory) {
        chatHistoryRepository.save(userChatHistory);
    }

    @Override
    public void deleteUserChat(UserChatHistory userChatHistory) {
        chatHistoryRepository.delete(userChatHistory);
    }
}
