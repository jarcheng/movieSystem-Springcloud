package com.jarcheng.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.jarcheng")
public class ChengWebsocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengWebsocketApplication.class, args);
    }

}
