package com.jarcheng.websocket.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
public class UserChatHistory {
    @Id
    String id;
    String myUUID;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    Date date;
    String toUser;
    String toUserName;
    String toUserImg;
    String lastMessage;
    Integer needCheck = 0;
}
