package com.jarcheng.websocket.controller;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.dao.UserDao;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserFollow;
import com.jarcheng.mbg.entity.User_ItemCollect;
import com.jarcheng.shiro.annotation.JwtIgnore;
import com.jarcheng.websocket.handler.MessageEventHandler;
import com.jarcheng.websocket.payload.SingleMessageRequest;
import com.jarcheng.websocket.payload.UserChatHistory;
import com.jarcheng.websocket.repository.UserChatMessageHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/chat")
@Slf4j
public class MessageController {
    @Autowired
    private MessageEventHandler messageHandler;
    @Autowired
    UserChatMessageHistoryRepository chatMessageHistoryRepository;
    @Autowired
    UserDao userDao;

    @PostMapping("/history")
    public Result getChatHistory(@RequestBody UserChatHistory userChatHistory) {
        log.info(userChatHistory.toString());
        List<SingleMessageRequest> history1 = chatMessageHistoryRepository.findSingleMessageRequestByFromUidAndToUid(userChatHistory.getToUser(), userChatHistory.getMyUUID());
        List<SingleMessageRequest> history2 = chatMessageHistoryRepository.findSingleMessageRequestByFromUidAndToUid(userChatHistory.getMyUUID(), userChatHistory.getToUser());
        log.info(history2.toString());
        log.info(history1.toString());
        history1.addAll(history2);
        history1.sort((o1, o2) -> {
            Date d1 = o1.getDate();
            Date d2 = o2.getDate();
            return d1.compareTo(d2);
        });
        return Result.SUCCESS(history1);
    }

    @JwtIgnore
    @PostMapping("/followerNotice")
    public void followerNotice(@RequestBody HashMap<String, Object> userFollow) {
        User user = userDao.selectByPrimaryKey((Integer) userFollow.get("user_id"));
        User user1 = userDao.selectByPrimaryKey((Integer) userFollow.get("follow"));
        user.setPassword("");
        user.setTelephone("");
        messageHandler.followNotice(user1.getName(), user);
    }

    @JwtIgnore
    @PostMapping("/likeNotice")
    public void likeNotice(@RequestBody User_ItemCollect userItemCollect) {
        messageHandler.likeNotice(userItemCollect.getUpName(), userItemCollect);
    }
//    @PostMapping("/broadcast")
//    public Dict broadcast(@RequestBody BroadcastMessageRequest message) {
//        if (isBlank(message)) {
//            return Dict.create().set("flag", false).set("code", 400).set("message", "参数为空");
//        }
//        messageHandler.sendToBroadcast(message);
//        return Dict.create().set("flag", true).set("code", 200).set("message", "发送成功");
//    }

    /**
     * 判断Bean是否为空对象或者空白字符串，空对象表示本身为<code>null</code>或者所有属性都为<code>null</code>
     *
     * @param bean Bean对象
     * @return 是否为空，<code>true</code> - 空 / <code>false</code> - 非空
     */
    private boolean isBlank(Object bean) {
        if (null != bean) {
            for (Field field : ReflectUtil.getFields(bean.getClass())) {
                Object fieldValue = ReflectUtil.getFieldValue(bean, field);
                if (null != fieldValue) {
                    if (fieldValue instanceof String && StrUtil.isNotBlank((String) fieldValue)) {
                        return false;
                    } else if (!(fieldValue instanceof String)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}