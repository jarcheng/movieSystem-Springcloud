package com.jarcheng.mbg.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRole implements Serializable {
    private Integer id;

    private String name;

    private String uuid;

    private String role;
    private Integer userId;
    private static final long serialVersionUID = 1L;
}