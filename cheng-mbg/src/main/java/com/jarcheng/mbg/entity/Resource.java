package com.jarcheng.mbg.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel
@Data
public class Resource {
    int id;
    Integer mid;
    String href;
    int episode;
}
