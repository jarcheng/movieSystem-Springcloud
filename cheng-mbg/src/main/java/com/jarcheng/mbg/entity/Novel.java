package com.jarcheng.mbg.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * novel
 * @author 
 */
@Data
public class Novel implements Serializable {
    private String name;

    private String author;

    private String description;

    private String img;

    private String href;

    private String category;

    private String status;

    private Integer chapter_count;

    private Date time;

    private static final long serialVersionUID = 1L;
}