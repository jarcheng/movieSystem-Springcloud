package com.jarcheng.mbg.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(description = "用户实体")
public class User implements Serializable {
    int id;
    String name;
    String telephone;
    String password;
    String gender;
    String signature;
    String img;
    String uuid;
    String background;
    String code;
    List<String> permissions;
    List<String> roles;
    String realName;
}
