package com.jarcheng.mbg.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * novel_chapter
 * @author 
 */
@Data
public class NovelChapter implements Serializable {
    private String NAME;

    private String novelName;

    private String chapter_href;

    private static final long serialVersionUID = 1L;
}