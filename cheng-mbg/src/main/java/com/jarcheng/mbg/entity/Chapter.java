package com.jarcheng.mbg.entity;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ApiModel
public class Chapter {
    String name;
    String chapter_href;
    String novelName;
    String content;
}
