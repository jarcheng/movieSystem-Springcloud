package com.jarcheng.mbg.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * user_follow
 *
 * @author
 */
@Data
public class UserFollow implements Serializable {
    private Integer id;

    private Integer user_id;

    private String uuid;

    private Integer follow;

    private List<User> followUsers;
    private static final long serialVersionUID = 1L;
}