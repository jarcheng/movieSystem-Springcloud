package com.jarcheng.mbg.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * order_payment
 *
 * @author
 */
@Data
public class OrderPayment implements Serializable {
    private Integer id;

    private String user_uuid;

    private String user_name;

    private String order_sn;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pay_time;
    private Integer orderId;
    private BigDecimal amount;

    private static final long serialVersionUID = 1L;
}