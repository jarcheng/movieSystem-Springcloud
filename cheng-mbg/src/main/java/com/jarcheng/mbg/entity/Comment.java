package com.jarcheng.mbg.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * comment
 *
 * @author
 */
@Data
public class Comment implements Serializable {
    private Integer id;
    private String uuid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date comment_date;
    private String comment_content;
    private String parent_uuid;
    private Integer parent_id;
    private Integer article_id;
    private User parent_user;
    private User user;
    private Integer userId;

    private static final long serialVersionUID = 1L;
}