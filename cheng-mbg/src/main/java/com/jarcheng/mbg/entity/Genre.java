package com.jarcheng.mbg.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * genre
 * @author 
 */
@Data
public class Genre implements Serializable {
    private String genre;

    private String big_genre;

    private static final long serialVersionUID = 1L;
}