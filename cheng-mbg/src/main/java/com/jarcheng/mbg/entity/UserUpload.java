package com.jarcheng.mbg.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * user_upload
 *
 * @author
 */
@Data
public class UserUpload implements Serializable {
    private Integer id;

    private String uuid;

    private String name;
    private Integer userId;


    /**
     * 用户上传的头像地
     */
    private String img;

    /**
     * 用户上传的背景图片地址
     */
    private String background;

    /**
     * 上传的视频地址
     */
    private String video;

    /**
     * 上传的文章地址
     */
    private String article;

    private Boolean isActive;

    private static final long serialVersionUID = 1L;
}