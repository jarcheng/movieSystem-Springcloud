package com.jarcheng.mbg.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * user_order
 *
 * @author
 */
@Data
public class UserOrder implements Serializable {
    private Integer id;
    private Integer userId;
    private String user_uuid;

    private String user_name;

    private String order_sn;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date create_time;

    private BigDecimal pay_amount;

    /**
     * 支付方式：0->未支付；1->支付宝；2->微信
     */
    private Integer pay_type;

    /**
     * 0->待付款,1->支付成功,2->关闭订单
     */
    private Integer status;

    private static final long serialVersionUID = 1L;
}