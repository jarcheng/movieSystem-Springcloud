package com.jarcheng.mbg.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;


/**
 * movie
 *
 * @author
 */

@Data
@ApiModel
//@Document(indexName = "movies", type = "movie")
public class Movie implements Serializable {
    private Integer id;
//    @Field(analyzer = "ik_smart", fielddata = true, type = FieldType.Text)
    private String name;
    private String href;
    private String img;
    private String play_href;
    private Integer year;
    private String language;
//    @Field(analyzer = "ik_smart", fielddata = true, type = FieldType.Text)
    private String storyline;
    private String genre;
    private String big_genre;
    private Integer rank;
    private String uuid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;
    private Integer userId;
    private Integer uploadId;
    User user;
    List<Resource> resourceList;
    private static final long serialVersionUID = 1L;
}