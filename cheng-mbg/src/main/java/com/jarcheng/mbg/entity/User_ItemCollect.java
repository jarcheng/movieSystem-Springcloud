package com.jarcheng.mbg.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class User_ItemCollect {
    //id 要换成string 要不然mongoDb无法字段生成
    @Id
    public String id;
    public Integer userId;
    @Indexed
    public String uuid;
    public String name;
    public String userImg;
    public Integer item_id;
    public String item_name;
    public String itemName;
    public String img;
    public String upUUID;
    public String upName;
    @ApiModelProperty("收藏/阅读的开始时间")
    public String time;
    @ApiModelProperty("计时器")
    public String timer;
    @JsonProperty("is_like")
    public boolean is_like;
    @JsonProperty("is_collect")
    public boolean is_collect;
    public String genre;
    public boolean checked;
}
