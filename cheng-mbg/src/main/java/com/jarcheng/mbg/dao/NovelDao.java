package com.jarcheng.mbg.dao;


import com.jarcheng.mbg.entity.Chapter;
import com.jarcheng.mbg.entity.Novel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface NovelDao {
    public List<Novel> getNovelByCate(Novel novel);

    public List<Novel> getNovelByName(Novel novel);

    public void deleteNovel(Novel novel);

    public void updateNovel(Novel novel);

    public void insertNovel(Novel novel);


    public List<Chapter> getChapter(Novel novel);
}
