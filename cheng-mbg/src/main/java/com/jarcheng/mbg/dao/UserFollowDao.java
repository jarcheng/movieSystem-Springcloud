package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.UserFollow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserFollowDao {
    int deleteByPrimaryKey(Integer id);

    int insert(UserFollow record);

    int insertSelective(UserFollow record);

    UserFollow selectByPrimaryKey(Integer id);

    List<UserFollow> selectAllFollow(String uuid);

    UserFollow selectSpecificFollow(@Param("uuid") String uuid, @Param("followId") Integer followId);

    int updateByPrimaryKeySelective(UserFollow record);

    int updateByPrimaryKey(UserFollow record);
}