package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.NovelChapter;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface NovelChapterDao {
    int insert(NovelChapter record);

    int insertSelective(NovelChapter record);
}