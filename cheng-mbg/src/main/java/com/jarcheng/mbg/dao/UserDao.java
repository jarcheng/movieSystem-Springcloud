package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserDao {
    int deleteByUUID(String uuid);

    int insert(User record);

    int insertSelective(User record);
    User selectByRealName(String realName);
    User selectByName(String name);
    User selectByPrimaryKey(Integer id);
    User selectByUUID(String uuid);

    User getUserPermission(@Param(value = "name") String name);

    List<User> selectAllUser();

    int updateByUUIDSelective(User record);

    int updateByPrimaryKey(User record);
}