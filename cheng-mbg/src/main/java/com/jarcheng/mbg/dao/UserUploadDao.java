package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.UserUpload;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserUploadDao {
    int deleteByPrimaryKey(Integer id);

    int insert(UserUpload record);

    int insertSelective(UserUpload record);

    UserUpload getAllVideo();

    UserUpload getAllArticle();

    UserUpload getUserVideo(String uuid);

    UserUpload getUserArticle(String uuid);

    UserUpload selectByPrimaryKey(Integer id);

    UserUpload getUserVideoByUrl(String url);

    int updateByPrimaryKeySelective(UserUpload record);

    int updateByPrimaryKey(UserUpload record);
}