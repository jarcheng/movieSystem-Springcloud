package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.Genre;
import com.jarcheng.mbg.entity.Movie;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
@Mapper
public interface MovieDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Movie record);

    int insertSelective(Movie record);

    Movie selectById(Integer id);

    List<Movie> selectAll();

    Movie listResource(@Param("table") String table,
                       @Param("id") String id);


    List<Movie> selectByGenre(String big_genre);

    List<Movie> selectUserVideo(String uuid);

    List<Genre> getGenres(String genre);

    int updateByPrimaryKeySelective(Movie record);

    int updateByPrimaryKey(Movie record);
}