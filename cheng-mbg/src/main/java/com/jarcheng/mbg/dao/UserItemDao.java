package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.User_ItemCollect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface UserItemDao {
    public List<User_ItemCollect> getItemCollected(User_ItemCollect userItemCollect);

    public List<User_ItemCollect> getItemLiked(User_ItemCollect userItemCollect);

    public List<User_ItemCollect> getUserAllCollect(User_ItemCollect userItemCollect);

    public List<User_ItemCollect> getUpAllBeLiked(@Param("uuid") String uuid);

    public List<User_ItemCollect> getUserAll(User_ItemCollect userItemCollect);

    public User_ItemCollect get(User_ItemCollect userItemCollect);


    public void update(User_ItemCollect userItemCollect);

    public void insert(User_ItemCollect userItemCollect);

    public void delete(User_ItemCollect userItemCollect);
}
