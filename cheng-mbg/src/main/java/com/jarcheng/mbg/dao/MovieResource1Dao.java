package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.Resource;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface MovieResource1Dao {
    int deleteByPrimaryKey(Integer id);

    int deleteByMid(Integer mid);

    int insert(Resource record);

    int insertSelective(Resource record);

    Resource selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Resource record);

    int updateByPrimaryKey(Resource record);
}