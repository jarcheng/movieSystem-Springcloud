package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.UserOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserOrderDao {


    int deleteByPrimaryKey(Integer id);

    int insert(UserOrder record);

    int insertSelective(UserOrder record);

    UserOrder selectByOrderSn(@Param("order_sn") String orderSn);

    List<UserOrder> selectByUserOrder(@Param("userUUID") String uuid);

    List<UserOrder> selectAll();

    UserOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserOrder record);

    int updateByPrimaryKey(UserOrder record);
}