package com.jarcheng.mbg.dao;

import com.jarcheng.mbg.entity.Genre;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface GenreDao {
    int insert(Genre record);

    int insertSelective(Genre record);
}