package com.jarcheng.mbg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChengMbgApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengMbgApplication.class, args);
    }

}
