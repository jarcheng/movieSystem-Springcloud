package com.jarcheng.upload.controller;

import cn.hutool.core.util.ReflectUtil;

import com.alibaba.fastjson.JSONObject;
import com.jarcheng.common.common.Result;
import com.jarcheng.common.enums.UploadEnum;
import com.jarcheng.mbg.dao.MovieDao;
import com.jarcheng.mbg.dao.UserDao;
import com.jarcheng.mbg.dao.UserUploadDao;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserUpload;
import com.jarcheng.shiro.annotation.JwtIgnore;
import com.jarcheng.upload.rabbitmq.UploadMqSendMessage;
import com.jarcheng.upload.service.MinioService;
import io.minio.MinioClient;
import io.minio.policy.PolicyType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Api(tags = "MinIO对象存储管理")
@RestController
@RequestMapping("/upload")
@Slf4j
public class UploadController {
    @Autowired
    UserDao userDao;
    @Autowired
    UserUploadDao uploadDao;
    @Autowired
    MinioService minioService;

    @Autowired
    MovieDao movieDao;
    @Autowired
    UploadMqSendMessage uploadMqSendMessage;

    @ApiOperation("图片上传")
    @PostMapping("/uploadImg")
    public Result uploadImg(@RequestParam("file") MultipartFile file) {
        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            String url = minioService.upload(file);
            UserUpload upload = new UserUpload();
            upload.setImg(url);
            upload.setName(user.getName());
            upload.setUuid(user.getUuid());
            upload.setUserId(user.getId());
            uploadDao.insertSelective(upload);
            Map<String, String> urlMap = new HashMap<>();
            urlMap.put("url", url);
            return Result.SUCCESS(urlMap);
        } catch (Exception e) {
            return Result.FAIL("上传失败");
        }
    }

    @ApiOperation("头像上传")
    @PostMapping("/uploadAvatar")
    public Result uploadAvatar(@RequestParam("file") MultipartFile file) {
        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            String url = minioService.upload(file);
            UserUpload upload = new UserUpload();
            upload.setImg(url);
            upload.setName(user.getName());
            upload.setUuid(user.getUuid());
            upload.setUserId(user.getId());
            uploadDao.insertSelective(upload);
            user.setImg(url);
            userDao.updateByUUIDSelective(user);
            Map<String, String> urlMap = new HashMap<>();
            urlMap.put("url", url);
            return Result.SUCCESS(urlMap);
        } catch (Exception e) {
            return Result.FAIL("上传失败");
        }
    }

    @ApiOperation("背景上传")
    @PostMapping("/uploadBackground")
    public Result uploadBackground(@RequestParam("file") MultipartFile file) {
        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            String url = minioService.upload(file);
            UserUpload upload = new UserUpload();
            upload.setBackground(url);
            upload.setName(user.getName());
            upload.setUuid(user.getUuid());
            upload.setUserId(user.getId());
            uploadDao.insertSelective(upload);
            user.setBackground(url);
            userDao.updateByUUIDSelective(user);
            Map<String, String> urlMap = new HashMap<>();
            urlMap.put("url", url);
            return Result.SUCCESS(urlMap);
        } catch (Exception e) {
            return Result.FAIL("上传失败");
        }
    }

    @ApiOperation("上传视频")
    @PostMapping("/uploadVideo")
    public Result uploadVideo(@RequestParam("file") MultipartFile file) throws Exception {
        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            String url = minioService.upload(file);
            UserUpload upload = new UserUpload();
            upload.setVideo(url);
            upload.setUserId(user.getId());
            upload.setName(user.getName());
            upload.setUuid(user.getUuid());
            uploadDao.insertSelective(upload);
            uploadMqSendMessage.send(url);
            Map<String, String> urlMap = new HashMap<>();
            urlMap.put("url", url);
            return Result.SUCCESS(urlMap);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.FAIL("上传失败");
        }
    }

    @ApiOperation("上传文章")
    @PostMapping("/uploadArticle")
    public Result uploadAricle(@RequestParam("file") MultipartFile file) {
        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            String url = minioService.upload(file);
            UserUpload upload = new UserUpload();
            upload.setArticle(url);
            upload.setName(user.getName());
            upload.setUserId(user.getId());
            upload.setUuid(user.getUuid());
            uploadDao.insertSelective(upload);
            return Result.SUCCESS("上传成功");
        } catch (Exception e) {
            return Result.FAIL("上传失败");
        }
    }

    @ApiOperation("删除")
    @GetMapping("/deleteObject")
    @JwtIgnore
    public Result deleteObject(@RequestParam("url") String url) {
        minioService.delete(url);
        return Result.SUCCESS();
    }
}
