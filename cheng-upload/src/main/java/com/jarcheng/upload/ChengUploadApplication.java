package com.jarcheng.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "com.jarcheng")
@EnableDiscoveryClient
public class ChengUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengUploadApplication.class, args);
    }

}
