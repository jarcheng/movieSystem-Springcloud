package com.jarcheng.upload.rabbitmq;

import com.jarcheng.common.enums.QueueEnum;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UploadMqConfig {
    @Bean
    public DirectExchange uploadExchange() {
        return (DirectExchange) ExchangeBuilder.
                directExchange(QueueEnum.QUEUE_UPLOAD_CANCEL.getExchange()).durable(true).build();
    }

    @Bean
    public Queue uploadQueue() {
        return new Queue(QueueEnum.QUEUE_UPLOAD_CANCEL.getName());
    }

    @Bean
    public Queue uploadTllQueue() {
        return QueueBuilder.durable(QueueEnum.QUEUE_TTL_ORDER_CANCEL.getName())
                .deadLetterExchange(QueueEnum.QUEUE_UPLOAD_CANCEL.getExchange())
                .deadLetterRoutingKey(QueueEnum.QUEUE_UPLOAD_CANCEL.getRouteKey())
                .ttl(600000).build();
    }

    @Bean
        //自动注入,参数对应方法名
    Binding uploadBinding(DirectExchange uploadExchange, Queue uploadQueue) {
        return BindingBuilder.bind(uploadQueue).to(uploadExchange).with(QueueEnum.QUEUE_UPLOAD_CANCEL.getRouteKey());
    }

    @Bean
    Binding uploadTllBinding(DirectExchange uploadExchange, Queue uploadTllQueue) {
        return BindingBuilder.bind(uploadTllQueue).to(uploadExchange).with(QueueEnum.QUEUE_TTL_UPLOAD_CANCEL.getRouteKey());

    }
}
