package com.jarcheng.upload.rabbitmq;

import com.jarcheng.mbg.dao.UserUploadDao;
import com.jarcheng.mbg.entity.UserUpload;
import com.jarcheng.upload.service.MinioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RabbitListener(queues = "movie.upload.cancel")
public class UploadMqReceiver {
    @Autowired
    MinioService minioService;

    @Autowired
    UserUploadDao userUploadDao;

    @RabbitHandler
    public void cancelUploadHandler(String url) {
        UserUpload userUpload = userUploadDao.getUserVideoByUrl(url);
        if (userUpload == null) return;
        if (!userUpload.getIsActive()) {
            userUploadDao.deleteByPrimaryKey(userUpload.getId());
            minioService.delete(url);
            log.info("超时未提交自动删除文件");
        }
    }
}
