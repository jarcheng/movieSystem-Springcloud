package com.jarcheng.upload.rabbitmq;

import com.jarcheng.common.enums.QueueEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UploadMqSendMessage {
    @Autowired
    RabbitTemplate rabbitTemplate;

    public void send(String url) {
        rabbitTemplate.convertAndSend(QueueEnum.QUEUE_TTL_UPLOAD_CANCEL.getExchange(),
                QueueEnum.QUEUE_TTL_UPLOAD_CANCEL.getRouteKey(), url);
    }
}
