package com.jarcheng.search.controller;


import com.jarcheng.search.service.EsMovieService;
import com.jarcheng.common.common.CommonPage;
import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.EsMovie;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "EsProductController", description = "搜索商品管理")
@RequestMapping("/esProduct")
public class EsMovieController {
    @Autowired
    EsMovieService esMovieService;

    @ApiOperation(value = "导入所有数据库中商品到ES")
    @GetMapping("/importAll")
    public Result<Integer> importAllList() {
        int count = esMovieService.importAll();
        return Result.SUCCESS(count);
    }

    @ApiOperation(value = "根据id删除商品")
    @GetMapping("/delete/{id}")
    public Result<Object> delete(@PathVariable Integer id) {
        esMovieService.delete(id);
        return Result.SUCCESS(null);
    }

    @ApiOperation(value = "根据id批量删除商品")
    @PostMapping("/delete/batch")
    public Result<Object> delete(@RequestParam("ids") List<Integer> ids) {
        esMovieService.delete(ids);
        return Result.SUCCESS(null);
    }

    @ApiOperation(value = "根据id创建商品")
    @RequestMapping(value = "/create/{id}", method = RequestMethod.POST)
    public Result<EsMovie> create(@PathVariable Integer id) {
        EsMovie esMovie = esMovieService.create(id);
        if (esMovie != null) {
            return Result.SUCCESS(esMovie);
        } else {
            return Result.FAIL();
        }
    }

    @ApiOperation(value = "简单搜索")
    @RequestMapping(value = "/search/simple", method = RequestMethod.GET)
    public Result<CommonPage<EsMovie>> search(@RequestParam(required = false) String keyword,
                                            @RequestParam(required = false, defaultValue = "0") Integer pageNum,
                                            @RequestParam(required = false, defaultValue = "5") Integer pageSize) {
        Page<EsMovie> esProductPage = esMovieService.findByNameOrStoryline(keyword, pageNum, pageSize);
        return Result.SUCCESS(CommonPage.restPage(esProductPage));
    }

}
