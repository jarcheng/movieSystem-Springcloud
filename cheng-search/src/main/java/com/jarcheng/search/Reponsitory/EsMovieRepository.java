package com.jarcheng.search.Reponsitory;

import com.jarcheng.mbg.entity.EsMovie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface EsMovieRepository extends ElasticsearchRepository<EsMovie, Integer> {
    /**
     * 搜索查询
     *
     * @param name 商品名称
     * @param page 分页信息
     */
    Page<EsMovie> findByNameOrStoryline(String name, String storyline, Pageable page);

    List<EsMovie> findByName(String name);

}
