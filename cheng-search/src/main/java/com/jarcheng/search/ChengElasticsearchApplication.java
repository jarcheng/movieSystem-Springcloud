package com.jarcheng.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.jarcheng")
public class ChengElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengElasticsearchApplication.class, args);
    }

}
