package com.jarcheng.search.service.impl;

;
import com.jarcheng.mbg.entity.Movie;
import com.jarcheng.search.Reponsitory.EsMovieRepository;
import com.jarcheng.search.service.EsMovieService;
import com.jarcheng.mbg.dao.MovieDao;
import com.jarcheng.mbg.entity.EsMovie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class EsMovieServiceImpl implements EsMovieService {
    @Autowired
    MovieDao movieDao;
    @Autowired
    EsMovieRepository movieRepository;

    public EsMovie MovieToEsMovie(Movie movie) {
        EsMovie esMovie = new EsMovie();
        esMovie.setBig_genre(movie.getBig_genre());
        esMovie.setGenre(movie.getGenre());
        esMovie.setHref(movie.getHref());
        esMovie.setId(movie.getId());
        esMovie.setImg(movie.getImg());
        esMovie.setLanguage(movie.getLanguage());
        esMovie.setName(movie.getName());
        esMovie.setPlay_href(movie.getPlay_href());
        esMovie.setRank(movie.getRank());
        esMovie.setResourceList(movie.getResourceList());
        esMovie.setStoryline(movie.getStoryline());
        esMovie.setTime(movie.getTime());
        esMovie.setUploadId(movie.getUploadId());
        esMovie.setUser(movie.getUser());
        esMovie.setUserId(movie.getUserId());
        esMovie.setUuid(movie.getUuid());
        esMovie.setYear(movie.getYear());
        return esMovie;
    }

    @Override
    public int importAll() {
        List<Movie> MovieList = movieDao.selectAll();
        List<EsMovie> EsMovieList = new ArrayList<>();
        for (Movie movie : MovieList) {
            EsMovieList.add(MovieToEsMovie(movie));
        }
        Iterable<EsMovie> esMovieIterable = movieRepository.saveAll(EsMovieList);
        Iterator<EsMovie> iterator = esMovieIterable.iterator();
        int result = 0;
        while (iterator.hasNext()) {
            result++;
            iterator.next();
        }
        return result;
    }

    @Override
    public void delete(Integer id) {
        movieRepository.deleteById(id);
    }

    @Override
    public EsMovie create(Integer id) {
        EsMovie result = null;
        EsMovie EsMovie = MovieToEsMovie(movieDao.selectById(id));
        if (EsMovie != null) {
            result = movieRepository.save(EsMovie);
        }
        return result;
    }

    @Override
    public void delete(List<Integer> ids) {
        if (!CollectionUtils.isEmpty(ids)) {
            List<EsMovie> esMovieList = new ArrayList<>();
            for (Integer id : ids) {

                EsMovie EsMovie = new EsMovie();
                EsMovie.setId(id);
                esMovieList.add(EsMovie);
            }
            movieRepository.deleteAll(esMovieList);
        }
    }

    @Override
    public Page<EsMovie> findByNameOrStoryline(String keyword, Integer pageNum, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        return movieRepository.findByNameOrStoryline(keyword, keyword, pageable);
    }

}
