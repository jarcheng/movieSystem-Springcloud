package com.jarcheng.search.service;

import com.jarcheng.mbg.entity.EsMovie;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EsMovieService {
    /**
     * 从数据库中导入所有商品到ES
     */
    int importAll();

    /**
     * 根据id删除商品
     */
    void delete(Integer id);

    /**
     * 根据id创建商品
     */
    EsMovie create(Integer id);

    /**
     * 批量删除商品
     */
    void delete(List<Integer> ids);

    /**
     * 根据关键字搜索名称或者副标题
     */
    Page<EsMovie> findByNameOrStoryline(String keyword, Integer pageNum, Integer pageSize);
}
