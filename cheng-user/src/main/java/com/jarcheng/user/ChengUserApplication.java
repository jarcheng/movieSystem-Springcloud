package com.jarcheng.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"com.jarcheng"})
@EnableDiscoveryClient
public class ChengUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengUserApplication.class, args);
    }

}
