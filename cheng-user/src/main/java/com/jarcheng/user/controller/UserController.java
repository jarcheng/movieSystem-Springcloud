package com.jarcheng.user.controller;

import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.shiro.annotation.JwtIgnore;
import com.jarcheng.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/user")
@Api(tags = "用户相关")
@Slf4j
public class UserController {
    @Autowired
    UserService userService;


    @PostMapping("/login")
    @ApiOperation("登录")
    @JwtIgnore
    public Result login(@RequestBody User user) {
        return userService.login(user);
    }

    @PostMapping("/faceLogin")
    @ApiOperation("人脸识别")
    @JwtIgnore
    public Result faceLogin(@RequestParam("file") MultipartFile file, @RequestParam("name") String name) throws Exception {
        return userService.faceLogin(file, name);
    }

    @PostMapping("/signUp")
    @JwtIgnore
    @ApiOperation("注册")
    public Result SignUp(@RequestBody User user) {
        return userService.signUp(user);
    }

    @GetMapping("/userinfo")
    public Result getUserInfo(@RequestParam(value = "uuid", defaultValue = "") String uuid) {

        return userService.getUserInfo(uuid);
    }

    @GetMapping("/listUser")
    @RequiresPermissions(value = "user:list")
    public Result listUser(@RequestParam(value = "start", defaultValue = "1") int start,
                           @RequestParam(value = "size", defaultValue = "10") int size) {
        return userService.listUsers(start, size);
    }

    @PostMapping("/deleteUser")
    @RequiresPermissions("user:delete")
    public Result deleteUser(@RequestBody User user) {
        userService.deleteUser(user.getUuid());
        return Result.SUCCESS();
    }

    @PostMapping("/updateUser")
    @RequiresPermissions("user:update")
    public Result updateUser(@RequestBody User user) {
        User user2 = (User) SecurityUtils.getSubject().getPrincipal();
        log.info(user.toString());
        user.setUuid(user2.getUuid());
        user.setName(user2.getName());
        userService.updateInfo(user);
        return Result.SUCCESS();
    }

    @ApiOperation("获取验证码")
    @GetMapping("/getAuthCode")
    @JwtIgnore
    public Result getAuthCode(@RequestParam String telephone) {

        return userService.getAuthCode(telephone);
    }

    @ApiOperation("判断验证码是否正确")
    @PostMapping("/verifyAuthCode")
    @JwtIgnore
    public Result verifyAuthCode(@RequestParam String telephone,
                                 @RequestParam String authCode) {
        return userService.verifyAuthCode(telephone, authCode);
    }
}