package com.jarcheng.user.controller;

import com.jarcheng.common.common.Result;
import com.jarcheng.common.common.ResultCode;
import com.jarcheng.mbg.entity.RolePermission;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserRole;
import com.jarcheng.user.service.RolePermissionService;
import com.jarcheng.user.service.UserRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@Api(tags = "权限")
@RequestMapping("/permission")
public class RolePermissionController {
    @Autowired
    RolePermissionService rolePermissionService;
    @Autowired
    UserRoleService userRoleService;

    @RequiresPermissions("permission:list")
    @GetMapping("/listPermission")
    public Result listPermission() {
        return rolePermissionService.listPermissions();
    }

    @RequiresRoles({"admin", "manager"})
    @PostMapping("/updatePermission")
    public Result updatePermission(@RequestBody RolePermission rolePermission) {
        return rolePermissionService.updatePermission(rolePermission);
    }

    @RequiresRoles("admin")
    @PostMapping("/insertPermission")
    public Result insertPermission(@RequestBody RolePermission rolePermission) {
        return rolePermissionService.insertPermission(rolePermission);
    }

    @RequiresRoles("admin")
    @GetMapping("/deletePermission")
    public Result deletePermission(@RequestBody RolePermission rolePermission) {
        return rolePermissionService.deletePermission(rolePermission);
    }

    @GetMapping("/listRole")
    @ApiOperation("列出所有身份")
    public Result<List<String>> listRole() {
        return userRoleService.listRole();
    }

    @PostMapping("/selectUserRole")
    @ApiOperation("查看用户身份")
    public Result<List<UserRole>> selectUserRole(@RequestParam(value = "uuid", defaultValue = "") String uuid) {
        if (uuid.equals("")) return new Result(ResultCode.PARAM_IS_INVALID);
        return userRoleService.selectUserRole(uuid);
    }

    @GetMapping("/selectUserRole2")
    @ApiOperation("查看用户身份2")
    public Result<List<UserRole>> selectUserRole() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        return userRoleService.selectUserRole(user.getUuid());
    }

    @PostMapping("/updateUserRole")
    @RequiresRoles("admin")
    public Result updateRole(@RequestBody UserRole userRole) {
        return userRoleService.update(userRole);
    }

    @PostMapping("/deleteRole")
    @ApiOperation("删除用户的身份")
    @RequiresRoles("admin")
    public Result<Integer> deleteRole(@RequestBody UserRole userRole) {
        log.info(userRole.toString());
        return userRoleService.deleteRole(userRole);
    }

    @PostMapping("/insertRole")
    @ApiOperation("添加用户的身份")
    @RequiresRoles("admin")
    public Result<Integer> insertRole(@RequestBody UserRole user_role) {
        return userRoleService.insertRole(user_role);
    }
}
