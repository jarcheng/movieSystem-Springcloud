package com.jarcheng.user.controller;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserFollow;
import com.jarcheng.user.service.UserFollowService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/follow")
@Slf4j
public class UserFollowController {
    @Autowired
    UserFollowService userFollowService;


    @PostMapping("/addFollow")
    public Result addFollow(@RequestBody UserFollow userFollow) {
        log.info(JSONObject.toJSONString(userFollow));
        HttpUtil.post("http://localhost:81/chat/followerNotice", JSONObject.toJSONString(userFollow));
        return Result.SUCCESS(userFollowService.addUserFollow(userFollow));
    }

    @GetMapping("/allFollows")
    public Result listFollow() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        return Result.SUCCESS(userFollowService.listFollow(user.getUuid()));
    }

    @GetMapping("/myFollow")
    public Result getFollow(@RequestParam("followId") Integer followId) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        return Result.SUCCESS(userFollowService.getSpecificFollow(user.getUuid(), followId));
    }

    @GetMapping("/deleteFollow")
    public Result deleteFollow(@RequestParam(value = "id", defaultValue = "") Integer id) {
        return Result.SUCCESS(userFollowService.deleteUserFollow(id));
    }
}
