package com.jarcheng.user.controller;

import com.jarcheng.common.common.Result;
import com.jarcheng.common.common.ResultCode;
import com.jarcheng.mbg.entity.Comment;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.user.service.CommentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/comment")
public class UserCommentController {
    @Autowired
    CommentService commentService;

    @PostMapping("/submitComment")
    public Result submitComment(@RequestBody Comment comment) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        comment.setUuid(user.getUuid());
        commentService.insertSelective(comment);
        return Result.SUCCESS();
    }

    @GetMapping("/getArticleComment")
    public Result getArticleComment(Integer articleId) {
        if (articleId != null) {
            return Result.SUCCESS(commentService.SelectArticleComments(articleId));
        } else return new Result(ResultCode.PARAM_TYPE_BIND_ERROR);
    }
}
