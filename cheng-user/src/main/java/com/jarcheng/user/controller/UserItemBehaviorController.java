package com.jarcheng.user.controller;


import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.jarcheng.common.common.Result;
import com.jarcheng.common.common.ResultCode;
import com.jarcheng.shiro.exception.handler.GlobalExceptionHandler;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.User_ItemCollect;
import com.jarcheng.user.service.impl.UserItemCollectServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RequestMapping("/collect")
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.PUT})
@Api(tags = "用户行为管理")
public class UserItemBehaviorController {
    @Autowired
    UserItemCollectServiceImpl userItemCollectService;

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @PostMapping("/insert")
    public Result addCollect(@RequestBody User_ItemCollect collect) throws IOException {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        collect.setUuid(user.getUuid());
        userItemCollectService.insert(collect);
        if (collect.is_like = true) {
            HttpUtil.post("http://localhost:81/chat/likeNotice", JSONObject.toJSONString(collect));
        }
        return new Result(ResultCode.SUCCESS);
    }


    @GetMapping("/getUserBeLiked")
    public Result getUserBeLiked() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        log.info(user.toString());
        return Result.SUCCESS(userItemCollectService.getUserBeLiked(user.getUuid()));
    }

    @PostMapping("/get")
    /*
     * 给普通用户查询的接口,需要uuid
     * */
    public Result getCollect(@RequestBody User_ItemCollect collect) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();

        collect.setUuid(user.getUuid());
        collect.setName(null);
        List<User_ItemCollect> c = userItemCollectService.getUserAllCollect(collect);

        if (c != null) {
            log.info(user.getName() + "的所有收藏" + c.toString());
            return Result.SUCCESS(c);
        }
        return Result.FAIL("用户对该item无行为记录");

    }

    @PostMapping("/updateCollects")
    public Result updateCollect(@RequestBody List<User_ItemCollect> collects) {
        for (User_ItemCollect collect : collects) {
            userItemCollectService.update(collect);
        }
        return Result.SUCCESS();
    }

    /*
     * 给普通用户查询的接口,需要uuid,查询当前用户的所有收藏
     * */
    @GetMapping("/getAll")
    public Result getUserCollect() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        User_ItemCollect collect = new User_ItemCollect();
        collect.setUuid(user.getUuid());
        List<User_ItemCollect> c = userItemCollectService.getUserAllCollect(collect);
        if (c != null) {
            return Result.SUCCESS(c);
        }
        return Result.FAIL("用户对该item无行为记录");
    }

    @PostMapping("/search")
    @RequiresPermissions({"collect:search"})
    public Result SearchUserCollect(@RequestBody User_ItemCollect collect) {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isPermitted("collect:search")) {
            List<User_ItemCollect> user_itemCollects = userItemCollectService.getUserAllCollect(collect);
            return Result.SUCCESS(user_itemCollects);
        } else return new Result(ResultCode.PERMISSION_UNAUTHORISE);
    }

    @PostMapping("/getBeCollected")
    public Result getItemBeCollected(@RequestBody User_ItemCollect collect) {
        List<User_ItemCollect> user_itemCollects = userItemCollectService.getItemCollected(collect);
        return Result.SUCCESS(user_itemCollects);
    }

    @PostMapping("/getItemBeLiked")
    public Result getItemBeLiked(@RequestBody User_ItemCollect collect) {
        List<User_ItemCollect> user_itemCollects = userItemCollectService.getItemLiked(collect);
        return Result.SUCCESS(user_itemCollects);
    }

    @PostMapping("/delete")
    public Result delete(@RequestBody User_ItemCollect collect) {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isPermitted("collect:delete")) {
            userItemCollectService.delete(collect);
            return Result.SUCCESS();
        } else return new Result(ResultCode.PERMISSION_UNAUTHORISE);
    }

    @ApiOperation("创建浏览记录")
    @PostMapping("/createHistory")
    public Result create(@RequestBody User_ItemCollect user_item) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        user_item.setUuid(user.getUuid());
        User_ItemCollect memberReadHistory = userItemCollectService.getItemHistory(user.getUuid(), user_item.getItem_name());

        if (memberReadHistory == null) {
            int count = userItemCollectService.create(user_item);
            if (count > 0) {
                return Result.SUCCESS(count);
            } else {
                return Result.FAIL();
            }
        }
        return Result.FAIL();
    }

    @ApiOperation("删除浏览记录")
    @RequestMapping(value = "/deleteHistory", method = RequestMethod.POST)
    public Result delete(@RequestParam("ids") List<String> ids) {
        int count = userItemCollectService.deleteHistory(ids);
        if (count > 0) {
            return Result.SUCCESS(count);
        } else {
            return Result.FAIL();
        }
    }

    @ApiOperation("展示浏览记录")
    @RequestMapping(value = "/listHistory", method = RequestMethod.GET)

    public Result<List<User_ItemCollect>> list() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        List<User_ItemCollect> memberReadHistoryList = userItemCollectService.list(user.getUuid());
        memberReadHistoryList.forEach((User_ItemCollect u) -> {
            u.setUuid("");
        });
        return Result.SUCCESS(memberReadHistoryList);
    }

    @ApiOperation("获取对应的记录")
    @RequestMapping(value = "/getItemHistory", method = RequestMethod.GET)

    public Result<List<User_ItemCollect>> getItemHistory(@RequestParam(value = "item_name", defaultValue = "") String item_name) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        User_ItemCollect memberReadHistory = userItemCollectService.getItemHistory(user.getUuid(), item_name);
        memberReadHistory.setUuid("");
        return Result.SUCCESS(memberReadHistory);
    }
}

