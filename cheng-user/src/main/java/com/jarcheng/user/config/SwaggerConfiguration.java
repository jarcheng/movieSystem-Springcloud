package com.jarcheng.user.config;

import com.jarcheng.shiro.annotation.JwtIgnore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Administrator
 * @version 1.0
 **/
@Configuration
@EnableWebMvc

@EnableSwagger2
@ComponentScan(basePackages = {"com.jarcheng.user.config"})
public class SwaggerConfiguration {

    @Bean
    public Docket buildDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(buildApiInfo())
                .select()
                // 要扫描的API(Controller)基础包
                .apis(RequestHandlerSelectors.basePackage("com.jarcheng.user.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * @param
     * @return springfox.documentation.service.ApiInfo
     * @Title: 构建API基本信息
     * @methodName: buildApiInfo
     */
    @JwtIgnore
    private ApiInfo buildApiInfo() {
        Contact contact = new Contact("起凡","","1507906763@qq.com");
        return new ApiInfoBuilder()
                .title("起凡")
                .description("影视网站")
                .contact(contact)
                .version("1.0.0").build();
    }

}