package com.jarcheng.user.repository;

import com.jarcheng.mbg.entity.User_ItemCollect;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface UserReadHistoryRepository extends MongoRepository<User_ItemCollect, String> {
    List<User_ItemCollect> findUser_ItemCollectByUuidOrderByTimeDesc(String uuid);

    User_ItemCollect findUser_ItemCollectByUuidAndItemName(String uuid, String itemName);
}
