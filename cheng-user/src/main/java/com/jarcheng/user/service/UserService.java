package com.jarcheng.user.service;


import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.User;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {
    public Result login(User user);

    public Result signUp(User user);

    public Result listUsers(int start, int size);

    public Result getUserInfo(String uuid);

    public User getUserInfoByID(Integer id);
    public Result deleteUser(String uuid);

    public Result updateInfo(User user);

    public Result getAuthCode(String phoneNumber);

    public Result verifyAuthCode(String phoneNumber, String code);

    public Result faceLogin(MultipartFile userFace, String name) throws Exception;

}
