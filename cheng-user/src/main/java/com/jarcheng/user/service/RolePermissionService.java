package com.jarcheng.user.service;

import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.RolePermission;

public interface RolePermissionService {
    Result listPermissions();

    Result updatePermission(RolePermission rolePermission);

    Result insertPermission(RolePermission rolePermission);

    Result deletePermission(RolePermission rolePermission);
}
