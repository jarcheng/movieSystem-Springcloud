package com.jarcheng.user.service;


import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.User_ItemCollect;

import java.util.List;

public interface UserItemCollectService {
    public List<User_ItemCollect> getItemCollected(User_ItemCollect userItemCollect);

    public List<User_ItemCollect> getItemLiked(User_ItemCollect userItemCollect);

    public List<User_ItemCollect> getUserBeLiked(String uuid);

    public User_ItemCollect get(User_ItemCollect userItemCollect);

    public List<User_ItemCollect> getUserAllCollect(User_ItemCollect userItemCollect);

    public List<User_ItemCollect> getUserAll(User_ItemCollect userItemCollect);

    public Result update(User_ItemCollect userItemCollect);

    public Result insert(User_ItemCollect userItemCollect);

    public Result delete(User_ItemCollect userItemCollect);

    /**
     * 生成浏览记录
     */
    int create(User_ItemCollect memberReadHistory);

    /**
     * 批量删除浏览记录
     */
    int deleteHistory(List<String> ids);

    /**
     * 获取用户浏览历史记录
     */
    List<User_ItemCollect> list(String uuid);

    User_ItemCollect getItemHistory(String uuid, String item_name);
}
