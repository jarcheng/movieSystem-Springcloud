package com.jarcheng.user.service.impl;

import com.jarcheng.user.repository.UserReadHistoryRepository;
import com.jarcheng.common.common.Result;
import com.jarcheng.common.common.ResultCode;
import com.jarcheng.mbg.dao.UserItemDao;
import com.jarcheng.mbg.entity.User_ItemCollect;
import com.jarcheng.user.service.UserItemCollectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserItemCollectServiceImpl implements UserItemCollectService {

    @Autowired
    UserItemDao userItemDao;

    @Autowired
    UserReadHistoryRepository userReadHistoryRepository;

    @Override
    public List<User_ItemCollect> getItemCollected(User_ItemCollect userItemCollect) {
        return userItemDao.getItemCollected(userItemCollect);
    }

    @Override
    public List<User_ItemCollect> getItemLiked(User_ItemCollect userItemCollect) {
        return userItemDao.getItemLiked(userItemCollect);
    }

    @Override
    public List<User_ItemCollect> getUserBeLiked(String uuid) {

        return userItemDao.getUpAllBeLiked(uuid);
    }

    @Override
    public List<User_ItemCollect> getUserAllCollect(User_ItemCollect userItemCollect) {
        return userItemDao.getUserAllCollect(userItemCollect);
    }

    @Override
    public List<User_ItemCollect> getUserAll(User_ItemCollect userItemCollect) {
        return userItemDao.getUserAll(userItemCollect);
    }

    @Override
    public User_ItemCollect get(User_ItemCollect userItemCollect) {
        return userItemDao.get(userItemCollect);
    }


    @Override
    public Result update(User_ItemCollect userItemCollect) {
        System.out.println(userItemCollect);
        boolean flag = true;
        List<User_ItemCollect> collect = userItemDao.getUserAll(userItemCollect);
        if (collect != null) {
            flag = false;
            userItemDao.update(userItemCollect);
        }
        if (flag) return new Result(ResultCode.PARAM_IS_INVALID);
        return Result.SUCCESS();
    }

    @Override
    public Result insert(User_ItemCollect userItemCollect) {
        List<User_ItemCollect> collect = userItemDao.getUserAll(userItemCollect);
        if (collect.isEmpty()) {
            userItemDao.insert(userItemCollect);
        } else {
            this.update(userItemCollect);
        }
        return Result.SUCCESS();
    }

    @Override
    public Result delete(User_ItemCollect userItemCollect) {
        userItemDao.delete(userItemCollect);
        return Result.SUCCESS();
    }

    @Override
    public int create(User_ItemCollect memberReadHistory) {
        memberReadHistory.setTime(String.valueOf(new Date().getTime()));
        userReadHistoryRepository.save(memberReadHistory);
        return 1;
    }

    @Override
    public int deleteHistory(List<String> ids) {
        List<User_ItemCollect> deleteList = new ArrayList<>();
        for (String id : ids) {
            User_ItemCollect user_item = new User_ItemCollect();
            user_item.setUserId(Integer.parseInt(id));
            deleteList.add(user_item);
        }
        userReadHistoryRepository.deleteAll(deleteList);
        return ids.size();
    }

    @Override
    public List<User_ItemCollect> list(String uuid) {
        return userReadHistoryRepository.findUser_ItemCollectByUuidOrderByTimeDesc(uuid);
    }

    @Override
    public User_ItemCollect getItemHistory(String uuid, String item_name) {
        return userReadHistoryRepository.findUser_ItemCollectByUuidAndItemName(uuid, item_name);
    }
}
