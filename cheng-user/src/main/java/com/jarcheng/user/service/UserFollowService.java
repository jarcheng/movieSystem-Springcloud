package com.jarcheng.user.service;

import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserFollow;

import java.util.List;

public interface UserFollowService {
    public List<UserFollow> listFollow(String uuid);

    public UserFollow getSpecificFollow(String uuid, Integer id);

    public int addUserFollow(UserFollow userFollow);

    public int deleteUserFollow(Integer id);
}
