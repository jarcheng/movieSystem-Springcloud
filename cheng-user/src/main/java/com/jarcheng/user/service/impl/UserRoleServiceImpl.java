package com.jarcheng.user.service.impl;

import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.dao.UserRoleDao;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserRole;
import com.jarcheng.user.service.UserRoleService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    UserRoleDao userRoleDao;

    @Override
    public Result listRole() {
        return Result.SUCCESS(userRoleDao.selectAll());
    }

    @Override
    public Result deleteRole(UserRole userRole) {
        return Result.SUCCESS(userRoleDao.deleteRole(userRole));
    }

    @Override
    public Result insertRole(UserRole userRole) {
        return Result.SUCCESS(userRoleDao.insertSelective(userRole));
    }

    @Override
    public Result update(UserRole userRole) {
        return Result.SUCCESS(userRoleDao.updateByPrimaryKeySelective(userRole));
    }

    @Override
    public Result selectUserRole(String uuid) {
        return Result.SUCCESS(userRoleDao.selectByPrimaryKey(uuid));
    }
}
