package com.jarcheng.user.service.impl;

import com.jarcheng.mbg.dao.CommentDao;
import com.jarcheng.mbg.entity.Comment;
import com.jarcheng.user.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentDao commentDao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return commentDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Comment record) {
        return commentDao.insert(record);
    }

    @Override
    public int insertSelective(Comment record) {
        return commentDao.insertSelective(record);
    }

    @Override
    public Comment selectByPrimaryKey(Integer id) {
        return commentDao.selectByPrimaryKey(id);
    }

    @Override
    public List<Comment> SelectArticleComments(Integer articleId) {
        return commentDao.SelectArticleComments(articleId);
    }

    @Override
    public int updateByPrimaryKeySelective(Comment record) {
        return commentDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Comment record) {
        return commentDao.updateByPrimaryKey(record);
    }
}
