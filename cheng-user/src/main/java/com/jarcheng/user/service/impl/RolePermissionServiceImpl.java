package com.jarcheng.user.service.impl;

import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.dao.RolePermissionDao;
import com.jarcheng.mbg.entity.RolePermission;
import com.jarcheng.user.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionServiceImpl implements RolePermissionService {
    @Autowired
    RolePermissionDao permissionDao;

    @Override
    public Result listPermissions() {
        return Result.SUCCESS(permissionDao.selectAll());
    }

    @Override
    public Result updatePermission(RolePermission rolePermission) {
        return Result.SUCCESS(permissionDao.updateByPrimaryKeySelective(rolePermission));
    }

    @Override
    public Result insertPermission(RolePermission rolePermission) {
        return Result.SUCCESS(permissionDao.insertSelective(rolePermission));
    }

    @Override
    public Result deletePermission(RolePermission rolePermission) {
        return Result.SUCCESS(permissionDao.deleteRolePermission(rolePermission));
    }

}
