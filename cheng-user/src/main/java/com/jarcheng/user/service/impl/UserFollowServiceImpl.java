package com.jarcheng.user.service.impl;

import com.jarcheng.mbg.dao.UserFollowDao;
import com.jarcheng.mbg.entity.UserFollow;
import com.jarcheng.user.service.UserFollowService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserFollowServiceImpl implements UserFollowService {
    @Autowired
    UserFollowDao userFollowDao;

    @Override
    public List<UserFollow> listFollow(String uuid) {
        return userFollowDao.selectAllFollow(uuid);
    }

    @Override
    public UserFollow getSpecificFollow(String uuid, Integer followId) {
        return userFollowDao.selectSpecificFollow(uuid, followId);
    }

    @Override
    public int addUserFollow(UserFollow userFollow) {
        return userFollowDao.insertSelective(userFollow);
    }

    @Override
    public int deleteUserFollow(Integer id) {
        return userFollowDao.deleteByPrimaryKey(id);
    }
}
