package com.jarcheng.user.service;

import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.User;
import com.jarcheng.mbg.entity.UserRole;

public interface UserRoleService {
    Result listRole();

    Result deleteRole(UserRole userRole);

    Result insertRole(UserRole userRole);

    Result update(UserRole userRole);
    Result selectUserRole(String uuid);
}
