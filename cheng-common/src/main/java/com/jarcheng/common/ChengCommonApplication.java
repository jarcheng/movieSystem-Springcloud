package com.jarcheng.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChengCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengCommonApplication.class, args);
    }

}
