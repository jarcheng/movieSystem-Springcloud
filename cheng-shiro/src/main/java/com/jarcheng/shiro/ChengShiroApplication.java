package com.jarcheng.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.jarcheng")
public class ChengShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengShiroApplication.class, args);
    }

}
