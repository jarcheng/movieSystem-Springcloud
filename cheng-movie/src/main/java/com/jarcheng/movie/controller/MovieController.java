package com.jarcheng.movie.controller;

import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.dao.UserUploadDao;
import com.jarcheng.mbg.entity.Movie;
import com.jarcheng.mbg.entity.Resource;
import com.jarcheng.mbg.entity.UserUpload;
import com.jarcheng.movie.service.MovieService;
import com.jarcheng.movie.service.ResourceService;
import com.jarcheng.shiro.annotation.JwtIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Api(tags = "影视资源相关")
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.PUT})
@Slf4j
@RequestMapping("/movie")
public class MovieController {
    @Autowired
    MovieService movieService;
    @Autowired
    ResourceService resourceService;

    @Autowired
    UserUploadDao userUploadDao;

    @Autowired
    RestTemplate restTemplate;
    String url = "http://cheng-upload";

    @ApiOperation("获取影视")
    @GetMapping("/items")
    @JwtIgnore
    public Result list(@RequestParam(value = "start", defaultValue = "1") int start,
                       @RequestParam(value = "size", defaultValue = "10") int size,
                       @RequestParam(value = "genre", defaultValue = "") String genre) {
        return movieService.listMovies(start, size, genre);

    }

    @ApiOperation("获取所有类别")
    @GetMapping("/genres")
    @JwtIgnore
    public Result getGenres(@RequestParam(value = "big_genre", defaultValue = "") String big_genre) {
        return movieService.listGenres(big_genre);
    }

    @ApiOperation("获取播放地址")
    @GetMapping(value = "/resource")
    public Result resource(@RequestParam(value = "id", defaultValue = "无") String id,
                           @RequestParam(value = "resource", defaultValue = "1") String resource) {
        return movieService.listResources(resource, id);
    }

    @ApiOperation("增加视频")
    @PostMapping("/insertMovie")
    public Result insertMovie(@RequestBody Map<String, String> map) {
        Movie movie = new Movie();
        UserUpload userUpload = userUploadDao.getUserVideoByUrl(map.get("href"));

        movie.setBig_genre(map.get("big_genre"));
        movie.setGenre(map.get("genre"));
        movie.setName(map.get("name"));
        movie.setImg(map.get("img"));
        movie.setStoryline(map.get("storyline"));
        movie.setUploadId(userUpload.getId());

        movieService.insetMovie(movie);
        userUpload.setIsActive(true);
        userUploadDao.updateByPrimaryKeySelective(userUpload);
        Resource resource = new Resource();
        resource.setMid(movie.getId());
        resource.setHref(map.get("href"));
        resource.setEpisode(Integer.parseInt(map.get("episode")));
        log.info(resource.toString());
        resourceService.insetResource(resource);
        return Result.SUCCESS();
    }

    @ApiOperation("删除视频")
    @PostMapping("/delete")
    @RequiresPermissions("movie:delete")
    public Result deleteVideo(@RequestBody Movie movie) {
        log.info("" + movie.getId());
        resourceService.deleteResourceByMid(movie.getId());

        movieService.deleteMovie(movie.getId());
        log.info(movie.toString());
        String video = userUploadDao.selectByPrimaryKey(movie.getUploadId()).getVideo();
        userUploadDao.deleteByPrimaryKey(movie.getUploadId());

        restTemplate.getForObject(url + "/upload/deleteObject?url=" + video, String.class);
        return Result.SUCCESS();
    }

    @ApiOperation("更新视频信息")
    @PostMapping("/update")
    @RequiresPermissions("movie:update")
    public Result updateVideo(@RequestBody Movie movie) {
        movieService.updateMovie(movie);
        return Result.SUCCESS();
    }

    @ApiOperation("获取用户的视频")
    @GetMapping("/userVideo")
    public Result getUserVidel(@RequestParam(value = "uuid",defaultValue = "")String uuid) {

        return Result.SUCCESS(movieService.getUserVideo(uuid));
    }
}

