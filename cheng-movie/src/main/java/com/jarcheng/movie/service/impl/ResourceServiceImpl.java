package com.jarcheng.movie.service.impl;

import com.jarcheng.mbg.dao.MovieResource1Dao;
import com.jarcheng.mbg.entity.Resource;
import com.jarcheng.movie.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResourceServiceImpl implements ResourceService {
    @Autowired
    MovieResource1Dao resource1Dao;

    @Override
    public int insetResource(Resource resource) {
        return resource1Dao.insertSelective(resource);
    }

    @Override
    public int deleteResourceByMid(Integer mid) {
        return resource1Dao.deleteByMid(mid);
    }
}
