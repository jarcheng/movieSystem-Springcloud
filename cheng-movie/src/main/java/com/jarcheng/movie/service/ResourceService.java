package com.jarcheng.movie.service;

import com.jarcheng.mbg.entity.Resource;
import org.springframework.stereotype.Service;

public interface ResourceService {
    int insetResource(Resource resource);

    int deleteResourceByMid(Integer id);
}
