package com.jarcheng.movie.service;


import com.jarcheng.common.common.Result;
import com.jarcheng.mbg.entity.Movie;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public interface MovieService {
    public Result listMovies(int start, int size, String genre);

    public Result listGenres(String bigGenre);

    public Result listResources(String index, String id);

    public int insetMovie(Movie movie);

    public int deleteMovie(int id);
    public int updateMovie(Movie movie);
    public List<Movie> getUserVideo(String uuid);
}
