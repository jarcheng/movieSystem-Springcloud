package com.jarcheng.movie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.jarcheng")
public class ChengMovieApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengMovieApplication.class, args);
    }

}
