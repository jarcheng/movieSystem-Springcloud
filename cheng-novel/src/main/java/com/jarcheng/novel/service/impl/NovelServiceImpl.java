package com.jarcheng.novel.service.impl;

import com.jarcheng.mbg.dao.NovelDao;
import com.jarcheng.mbg.entity.Chapter;
import com.jarcheng.mbg.entity.Novel;
import com.jarcheng.novel.service.NovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class NovelServiceImpl implements NovelService {
    NovelDao novelDao;

    @Autowired
    public NovelServiceImpl(NovelDao novelDao) {
        this.novelDao = novelDao;
    }

    @Override
    public List<Novel> getNovelByCate(Novel novel) {
        return novelDao.getNovelByCate(novel);
    }

    @Override
    public List<Novel> getNovelByName(Novel novel) {
        return novelDao.getNovelByName(novel);
    }

    @Override
    public void deleteNovel(Novel novel) {
        novelDao.deleteNovel(novel);

    }

    @Override
    public void updateNovel(Novel novel) {
        novelDao.updateNovel(novel);
    }

    @Override
    public void insertNovel(Novel novel) {
        novelDao.insertNovel(novel);
    }


    @Override
    public List<Chapter> getChapter(Novel novel) {
        return novelDao.getChapter(novel);
    }
}
