package com.jarcheng.novel.service;



import com.jarcheng.mbg.entity.Chapter;
import com.jarcheng.mbg.entity.Novel;

import java.util.List;

public interface NovelService {
    public List<Novel> getNovelByCate(Novel novel);

    public List<Novel> getNovelByName(Novel novel);

    public void deleteNovel(Novel novel);

    public void updateNovel(Novel novel);

    public void insertNovel(Novel novel);
    public List<Chapter> getChapter(Novel novel);

}
