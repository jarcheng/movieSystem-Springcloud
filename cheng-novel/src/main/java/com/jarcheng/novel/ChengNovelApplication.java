package com.jarcheng.novel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.jarcheng")
public class ChengNovelApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChengNovelApplication.class, args);
    }

}
