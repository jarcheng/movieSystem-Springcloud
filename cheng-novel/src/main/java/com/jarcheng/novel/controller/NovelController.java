package com.jarcheng.novel.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jarcheng.common.common.Result;
import com.jarcheng.common.common.ResultCode;
import com.jarcheng.mbg.entity.Chapter;
import com.jarcheng.mbg.entity.Novel;
import com.jarcheng.novel.service.NovelService;
import com.jarcheng.shiro.annotation.JwtIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/novel")
@RestController
@Api(tags = "NovelController", description = "小说增删改查")
public class NovelController {
    NovelService novelService;

    @Autowired
    public NovelController(NovelService novelService) {
        this.novelService = novelService;
    }

    @GetMapping("/byCate")
    @JwtIgnore
    @ApiOperation("按类别查找小说")
    public Result list(@RequestParam(value = "start", defaultValue = "1") int start,
                       @RequestParam(value = "size", defaultValue = "10") int size,
                       @RequestParam(value = "category", defaultValue = "") String category,
                       HttpServletRequest httpServletRequest) throws Exception {

        Novel novel = new Novel();
        novel.setCategory(category);
        PageInfo<Novel> page = null;
        PageHelper.startPage(start, size);
        List<Novel> ms = novelService.getNovelByCate(novel);
        page = new PageInfo<>(ms, 5);
        return Result.SUCCESS(page);
    }

    @PostMapping("/byName")
    @JwtIgnore
    @ApiOperation("按名字查找小说")

    public Result getNovelByName(@RequestBody Novel novel) {
        return Result.SUCCESS(novelService.getNovelByName(novel));
    }


    @PostMapping("/update")
    @RequiresPermissions({"manager", "admin"})
    @ApiOperation("修改小说")
    public Result update(@RequestBody Novel novel) {
//        System.out.println(novel);
//        if (SecurityUtils.getSubject().isPermitted("novel:update")) {
//            novelService.updateNovel(novel);
//        }
        novelService.updateNovel(novel);

        return Result.SUCCESS();
    }

    @PostMapping("/delete")
    @RequiresPermissions({"admin"})
    @ApiOperation("删除小说")
    public Result delete(@RequestBody Novel novel) {
        novelService.deleteNovel(novel);
        return Result.SUCCESS();
//        if (SecurityUtils.getSubject().isPermitted("novel:delete")) {
//            novelService.deleteNovel(novel);
//            return Result.SUCCESS();
//        } else {
//            return new Result(ResultCode.PERMISSION_UNAUTHORISE);
//        }
    }

    @PostMapping("/insert")
    @ApiOperation("增加小说")
    public Result insert(@RequestBody Novel novel) {
        if (SecurityUtils.getSubject().isPermitted("novel:insert")) {
            novelService.insertNovel(novel);
            return Result.SUCCESS();
        } else {
            return new Result(ResultCode.PERMISSION_UNAUTHORISE);
        }
    }

    @GetMapping("/chapter")
    @JwtIgnore
    @ApiOperation("获取小说对应的章节")
    public Result chapter(@RequestParam(value = "start", defaultValue = "1") int start,
                          @RequestParam(value = "size", defaultValue = "10") int size,
                          @RequestParam(value = "name", defaultValue = "") String name) throws Exception {
        Novel novel = new Novel();
        novel.setName(name);
        PageInfo<Chapter> page = null;
        PageHelper.startPage(start, size);
        List<Chapter> ms = novelService.getChapter(novel);
        page = new PageInfo<>(ms, 5);
        return Result.SUCCESS(page);
    }
//    public Result chapter(@RequestParam("name") String name) {
//        Novel novel = new Novel();
//        novel.setName(name);
//
//        return Result.SUCCESS(novelService.getChapter(novel));
//    }
}
